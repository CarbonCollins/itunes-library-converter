import os from 'os';
import glob from 'glob';
import path from 'path';
import fs from 'fs-extra';
import { exec } from 'child_process';
import asyncPool from 'tiny-async-pool';

const baseDir = 'C:/Users/Carbon/Music/iTunes/iTunes Media/Music';
const outputDir = 'C:/Users/Carbon/Music/Test';
const inputGlob = '**/**/*.m4a';

function getCoreCount() {
  return os.cpus().length || 1;
}

function getGlobFiles(globString, options = {}) {
  return new Promise((resolve, reject) => {
    glob(globString, options, (err, files) => {
      if (err) {
        reject(err);
      } else {
        resolve(files || []);
      }
    });
  });
}

function execPromise(command) {
  return new Promise((resolve, reject) => {
    exec(command, (err, stdout, stderr) => {
      if (err || stderr) {
        reject(err || stderr);
      } else {
        resolve(stdout);
      }
    });
  });
}

getGlobFiles(inputGlob, { cwd: baseDir })
  .then((files) => {
    return getGlobFiles(inputGlob.replace('.m4a', '.mp3'), { cwd: outputDir })
      .then((distFiles) => {
        const completeFiles = distFiles.map((file) => {
          return file.replace('.mp3', '.m4a')
        });

        return files.filter((file) => {
          return !completeFiles.includes(file);
        })
      });
  })
  .then((files) => {
    return files.map((file) => {
      const source = path.join(baseDir, file);
      const target = path.join(outputDir, file).replace('.m4a', '.mp3');

      return {
        source,
        target,
        command: `transcoder "${source}" "${target}"`
      }
    });
  })
  .then((files) => {
    const transcodePromise = (input) => {
      console.log(`transcoding ${input.source}`);

      return fs.ensureDir(path.dirname(input.target))
        .then(() => {
          return execPromise(input.command);
        });
    }
    return asyncPool(6, files, transcodePromise);
  })
  .then((result) => {
    console.log('done');
    console.log(result);
  })
  .catch(console.error);